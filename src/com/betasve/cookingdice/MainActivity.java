package com.betasve.cookingdice;

import java.util.Random;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	private String[] cm_array;
	private String[] meat_array;
	private String[] summer_array;
	private String[] grain_array;
	private Button roll_button;
	private TextView cm_text;
	private TextView meat_text;
	private TextView summer_text;
	private TextView grain_text;
	private ImageView cm_dice;
	private ImageView meat_dice;
	private ImageView summer_dice;
	private ImageView grain_dice;
	private TextView feedback;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        // Get views from the layout
        cm_text = (TextView) findViewById(R.id.cm);
        meat_text = (TextView) findViewById(R.id.meat);
        summer_text = (TextView) findViewById(R.id.summer);
        grain_text = (TextView) findViewById(R.id.grain);
        
        cm_dice = (ImageView) findViewById(R.id.method_dice);
        meat_dice = (ImageView) findViewById(R.id.meat_dice);
        summer_dice = (ImageView) findViewById(R.id.summer_dice);
        grain_dice = (ImageView) findViewById(R.id.grain_dice);
        
        roll_button = (Button) findViewById(R.id.roll);
        feedback = (TextView) findViewById(R.id.feedback);
        
        // Get dice arrays from the values strings
        cm_array = getResources().getStringArray(R.array.cooking_method);
        meat_array = getResources().getStringArray(R.array.meat);
        summer_array = getResources().getStringArray(R.array.summer);
        grain_array = getResources().getStringArray(R.array.grain);
        
        feedback.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent feedback = new Intent(MainActivity.this, FeedbackActivity.class);
				startActivity(feedback);
			}
		});
    }

    public void throwDice(View v){
    	Random r = new Random();
    	int random_cm = r.nextInt(6);
    	int random_meat = r.nextInt(6);
    	int random_summer = r.nextInt(6);
    	int random_grain = r.nextInt(6);
    	
    	String cm_choice = cm_array[random_cm].toString();
    	String meat_choice = meat_array[random_meat].toString();
    	String summer_choice = summer_array[random_summer].toString();
    	String grain_choice = grain_array[random_grain].toString();
    	
    	cm_text.setText(cm_choice);
    	meat_text.setText(meat_choice);
    	summer_text.setText(summer_choice);
    	grain_text.setText(grain_choice);
    	
    	String cm_drawable = "methods_" + cm_choice.toLowerCase() + "_dice";
    	int cm_drawable_int = getResources().getIdentifier(cm_drawable, "drawable", getPackageName());
    	cm_dice.setImageResource(cm_drawable_int);
    	
    	String meat_drawable = "meats_" + meat_choice.toLowerCase() + "_dice";
    	int meat_drawable_int = getResources().getIdentifier(meat_drawable, "drawable", getPackageName());
    	meat_dice.setImageResource(meat_drawable_int);
    	
    	String summer_drawable = "summers_" + summer_choice.toLowerCase() + "_dice";
    	int summer_drawable_int = getResources().getIdentifier(summer_drawable, "drawable", getPackageName());
    	summer_dice.setImageResource(summer_drawable_int);
    	
    	String grain_drawable = "grains_" + grain_choice.toLowerCase() + "_dice";
    	int grain_drawable_int = getResources().getIdentifier(grain_drawable, "drawable", getPackageName());
    	grain_dice.setImageResource(grain_drawable_int);
    	
    	/*cm_text.setText(String.valueOf(random_cm));
    	meat_text.setText(String.valueOf(random_meat));
    	summer_text.setText(String.valueOf(random_summer));
    	grain_text.setText(String.valueOf(random_grain));*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
